#include <stdio.h>

int main(){
	double deposito, deposito_inicial, juros, rendimento_mensal, total_investimento;
	int i,meses;
	scanf("%lf %lf %d",&deposito, &juros, &meses);
	deposito_inicial = deposito;
	for(i = 0; i<meses; i++){
		rendimento_mensal = deposito*juros/100.00;
		printf("Rendimento no mes %d: %.2f\n",i,rendimento_mensal);
		deposito = deposito + rendimento_mensal;
		total_investimento = total_investimento + rendimento_mensal;

	}
	total_investimento = total_investimento + deposito_inicial;
	printf("Saldo final do investimento: %.2lf\n",total_investimento);

return 0;
}
