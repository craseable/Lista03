#include <stdio.h>

int validaDia(int dia){

	while(dia<=0 || dia >30){
		scanf("%d",&dia);
	
	}
	return dia;
}

int validaMes(int mes){
	while(mes<=0 || mes>12){
		scanf("%d",&mes);
	}
	return mes;
}

int validaAno(int ano){
	
	while(ano<=0 || ano >= 2020){
		scanf("%d",&ano);
	}
	return ano;
}

int main(){
	int dn,mn,an,dh,mh,ah,i;
	int dia=0,mes,anos,dias_totais=0;
	scanf("%d",&dn);
	validaDia(dn);
	scanf("%d",&mn);
	validaMes(mn);
	scanf("%d",&an);
	validaAno(an);
	scanf("%d",&dh);
	validaDia(dh);
	scanf("%d",&mh);
	validaMes(mh);
	scanf("%d",&ah);
	validaAno(ah);
	while(ah < an){
		scanf("%d",&ah);
	}
	if( ah == an ){
		while(mh < mn){
			scanf("%d",&mh);
		}
		if(mh == mn){
			while(dh < dn){
				scanf("%d",&dh);
			}
		}
	}
	anos = an;
	mes = mn;
	dia = dn;
	while(anos<ah){
		if(mes==12){
			if(dia<30){
				dia++;
				dias_totais++;
				
			}
			else if(dia==30){
				mes = 1;
				dia = 1;
				anos++;
				dias_totais++;
			} 

		}
		else if(mes < 12){
			if(dia < 30){
				dia++;
				dias_totais++;
			}
			if(dia==30){
				dia = 1;
				mes++;
				dias_totais++;
			}
		}
	}
	mes = mn;
	dia = dn;
	if(ah == an){
		while(mes<mh){
			if(dia==30){
				dia = 1;
				dias_totais++;
				mes++;
                	}
                	else if(dia<30){
                        	dia++;
				dias_totais++;
                        }
		dia = dn;
			if(mh==mn){
				while(dia<dh){
					dia++;
					dias_totais++;
				}
			}		
		}
	}
	printf("\n\n%d/%d/%d\n\n%d/%d/%d\n\n",dn,mn,an,dh,mh,ah);	
	printf("%d \n",dias_totais);
	

	return 0;
}
