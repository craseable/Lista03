#include <stdio.h>

int main(){
	double salario=1.0;
	double ajuste_total=0.0,ajuste;
	do{
		scanf("%lf",&salario);
		if(salario < 0){
			break;
		}
		if(salario < 500){
			salario = salario*1.15;
			ajuste = salario*0.15;
		}
		else if(salario >=500 && salario <= 1000){
			salario = salario*1.10;
			ajuste = salario*0.10;	
		}
		else if(salario > 1000){
			salario = salario*1.05;
			ajuste = salario*0.05;
		}
		ajuste_total += ajuste;
		printf("%.2f ",salario);
	}while(salario>=0);
	printf(" %.2f \n",ajuste_total);




	return 0;
}
